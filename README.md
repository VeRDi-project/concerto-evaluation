This repository contains the code of the experiments presented in our paper,
as well as the results we obtained. Two types of experiments were conducted:
synthetic experiments (`synthetic` folder) and experiments on a real use case
(`galera` folder). Each of these folders contain two sub-folders: `code`
contains the code and instructions to repeat our experiments and analyze the
results, while `results` contains the results we obtained and used in the
paper. Please refer to the `README` files of each `code` folders for more
information on each experiment.
